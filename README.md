# [Avem HUD](https://but0n.github.io/Avem_HUD/index.html)

### Avem HUD FUI Test

Version [c060366](https://github.com/but0n/Avem_HUD/tree/c060366abdbc1f1387240b0392a182dedd4af9f8):

Render the euler data from built-in IMU of mobile device

![](demo.PNG)

### Host Test
Version [5226417](https://github.com/but0n/Avem_HUD/tree/522641797b0ff9c32111132ed3fec104b7fff801):

A web client to render the euler data from the [Avem](https://github.com/but0n/Avem) flight controller board

![](host.gif)

Unfortunately, the motion response have a little bit delay as the angles increase, I probably should use the built-in DMP of MPU6050